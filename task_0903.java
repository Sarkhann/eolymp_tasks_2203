import java.util.Scanner;

public class task_0903 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int a = n % 10;
        int b = n / 100;
        if (a > b)
            System.out.println(a);
        else if (b > a)
            System.out.println(b);
        else
            System.out.println("=");
    }
}
