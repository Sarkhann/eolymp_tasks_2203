import java.util.Scanner;
public class task_8628 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int a = n%10;
        int b = n/10%10;
        int c = n/100%10;
        int d = n/1000;
        if(a%2==0 && b%2==0 && c%2==0 && d%2==0)
            System.out.println("YES");
        else
            System.out.println("NO");
    }
}
