import java.util.Scanner;
public class task_8893 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        long n = scan.nextLong();

        if (n%3==0 && n%2==0 && (n>9 && n<=99) || n>=-99 && n<-9 )
            System.out.println("YES");
        else
            System.out.println("NO");

    }
}