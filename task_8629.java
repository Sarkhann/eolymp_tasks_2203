import java.util.Scanner;
public class task_8629 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int a = n%10;
        int b = n/10%10;
        int c = n/100%10;
        int d = n/1000;
        if(a%2==1 || b%2==1 || c%2==1 || d%2==1)
            System.out.println("YES");
        else
            System.out.println("NO");
    }
}
