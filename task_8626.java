import java.util.Scanner;
public class task_8626 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int n = 0, m = 0;
        boolean t = false;
        while (a > 0) {
            n = a % 10;
            m = a / 10 % 10;
            a = a / 10;
            if (m == 3 && n == 7) {
                t = true;
                break;
            }
        }
        if (t==true)
            System.out.println("YES");

        else
            System.out.println("NO");
    }
}
