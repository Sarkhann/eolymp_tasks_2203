import java.util.Scanner;
public class task_8892 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        long n = scan.nextLong();
        long m = 0;
        if (n<0)
            m = n * (-1);
        else
            m=n;

        if(m%2==1 || (n > 0 && (n>99 && n<=999)))
            System.out.println("YES");

        else
            System.out.println("NO");

    }
}
