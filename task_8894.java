import java.util.Scanner;
public class task_8894 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        long n = scan.nextLong();

        if ((n%2==0 && n>=0) || (n%2!=0 && n<0))
            System.out.println("NO");
        else
            System.out.println("YES");

    }
}